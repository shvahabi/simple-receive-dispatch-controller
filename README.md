# SRD Controller
## Introduction
SRD (Simple Receive Dispatch) is a **dockerized 3-tier microservices RESTful MVC web application** developed and maintained by [BSPTeams (Sustainable Constructive Engineering)](http://www.bspteams.com/) for [Sardkhaneh Farrokh](https://sardkhaneh.com/). This repository includes Controller module of MVC based on [Scalatra](http://scalatra.org), [Argonaut](http://argonaut.io/) and [ScalikeJDBC](http://scalikejdbc.org/) frameworks.
## Instructions
This project benefits from a docker ecosystem to compile, run and setup a microservice. Note that the other two modules namely, [SRD View](https://gitlab.com/shvahabi/simple-receive-dispatch-view) and [SRD Model](https://gitlab.com/shvahabi/simple-receive-dispatch-model), shall be up and running in their respective containers for SRD Controller to function properly. For simplicity, this documentation assumes current working directory is `/home/noema/shahed/gitlab`.
### Prerequisites
1. Pull docker image from dockerhub:
	- `docker pull shvahabi/fullstackdevtool:0.4`
1. Check available networks to docker:
	- `docker network ls`
1. Create network **bsp** if it does not exist:
	- `docker network create --subnet=172.18.0.0/16 bsp`
1. Clone this repository:
	- `git clone -b latest --depth 1 https://gitlab.com/shvahabi/simple-receive-dispatch-controller.git /home/noema/shahed/gitlab/simple-receive-dispatch-controller`

in which `latest` is an ever-changing tag always pointing to intended version
### Compile Source
First of all, get rid of previous compilations:
- `sudo rm -fr /home/noema/shahed/gitlab/simple-receive-dispatch-controller/project/project /home/noema/shahed/gitlab/simple-receive-dispatch-controller/project/target /home/noema/shahed/gitlab/simple-receive-dispatch-controller/target; sudo rm -fr /home/noema/shahed/gitlab/simple-receive-dispatch-controller/release; sudo chmod -R 777 /home/noema/shahed/gitlab/simple-receive-dispatch-controller;`

then
- If you prefer to use local sbt and ivy repositories (recommended):
	- `docker run --rm -v /home/noema/shahed/gitlab/simple-receive-dispatch-controller:/git -v ~/.sbt:/root/.sbt -v ~/.ivy2:/root/.ivy2 shvahabi/fullstackdevtool:0.4 sbt "set offline := true" clean reload update evicted compile package; sudo mkdir /home/noema/shahed/gitlab/simple-receive-dispatch-controller/release; sudo mv /home/noema/shahed/gitlab/simple-receive-dispatch-controller/target/scala*/*.war /home/noema/shahed/gitlab/simple-receive-dispatch-controller/release/ROOT.war`
- If you prefer to use latest updates of sbt/scala:
	- `docker run --rm -v /home/noema/shahed/gitlab/simple-receive-dispatch-controller:/git -v ~/.sbt:/root/.sbt -v ~/.ivy2:/root/.ivy2 shvahabi/fullstackdevtool:0.4 sbt clean reload update evicted compile package; sudo mkdir /home/noema/shahed/gitlab/simple-receive-dispatch-controller/release; sudo mv /home/noema/shahed/gitlab/simple-receive-dispatch-controller/target/scala*/*.war /home/noema/shahed/gitlab/simple-receive-dispatch-controller/release/ROOT.war`
### Run
Run docker microservice:
- `docker run --net bsp --ip 172.18.0.11 -d -v /home/noema/shahed/gitlab/simple-receive-dispatch-controller/release:/var/lib/jetty9/webapps -p 3002:8080 --name SRD_Controller shvahabi/srdc:0.2`
## Single Liner for New Release (no compilation)
Execute the following single line for every new release:
- `docker rm -f SRD_Controller; sudo rm -fr /home/noema/shahed/gitlab/simple-receive-dispatch-controller; git clone -b package --depth 1 https://gitlab.com/shvahabi/simple-receive-dispatch-controller.git /home/noema/shahed/gitlab/simple-receive-dispatch-controller; sudo rm -fr /home/noema/shahed/gitlab/simple-receive-dispatch-controller/.git; sudo mkdir /home/noema/shahed/gitlab/simple-receive-dispatch-controller/release; sudo mv /home/noema/shahed/gitlab/simple-receive-dispatch-controller/*.war /home/noema/shahed/gitlab/simple-receive-dispatch-controller/release/ROOT.war; sudo chmod -R 777 /home/noema/shahed/gitlab/simple-receive-dispatch-controller; docker run --net bsp --ip 172.18.0.11 -d -v /home/noema/shahed/gitlab/simple-receive-dispatch-controller/release:/var/lib/jetty9/webapps -p 3002:8080 --name SRD_Controller shvahabi/srdc:0.2`
## Single Liner for Hardware Restart (Power Outage)
Execute the following single line to recover after power failure:
- `docker start SRD_Controller`
## Notes
- Controller container IP:PORT in bsp network is [172.18.0.11:8080](http://172.18.0.11:8080) binded to host as [127.0.0.1:3002](http://127.0.0.1:3002/).
