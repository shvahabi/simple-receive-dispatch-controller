package com.bsp.receivedispatch.controller

import com.bsp.receivedispatch.controller.orm.ui.Person
import com.bsp.receivedispatch.controller.orm.{DispatchingTransaction, ReceivingTransaction}
import org.scalatra._
import org.scalatra.CorsSupport
import scalikejdbc._
import argonaut.Argonaut._
import argonaut.Json
import org.scalatra.ActionResult._


class ReceiveDispatchServlet extends ScalatraServlet with CorsSupport {

  before() {
    Class.forName("org.h2.Driver")
    ConnectionPool.singleton("jdbc:h2:tcp://172.18.0.10:9092//db/DB", "foo", "bar")
    GlobalSettings.loggingSQLAndTime = LoggingSQLAndTimeSettings(
      enabled = false,
      singleLineMode = false
    )
  }
  options("/*"){
    response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"))
  }

  get("/receivedispatch/receipts/:id") {
    val errorMessage: String = "هیچ فرم ورود کالایی با چنین شماره\u200Cای وجود ندارد."
    (DB readOnly { implicit session => sql"SELECT `submitted` FROM `forms` JOIN `received` ON `forms`.`no` = `received`.`form` WHERE `forms`.`no` = ${params("id")}".map(rs => rs.string("submitted")).single.apply })
      .getOrElse(<h1 dir="rtl" lang="fa">{errorMessage}</h1>)

  }
  get("/receivedispatch/dispatches/:id") {
    val errorMessage: String = "هیچ فرم خروج کالایی با چنین شماره\u200Cای وجود ندارد."
    (DB readOnly { implicit session => sql"SELECT `submitted` FROM `forms` JOIN `dispatched` ON `forms`.`no` = `dispatched`.`form` WHERE `forms`.`no` = ${params("id")}".map(rs => rs.string("submitted")).single.apply })
      .getOrElse(<h1 dir="rtl" lang="fa">{errorMessage}</h1>
      )
  }
  post("/receivedispatch/receipts") {
    DB localTx { implicit session =>
      SQL(ReceivingTransaction(request.body.intern).toSql).execute().apply()
    }
    Ok
  }
  post("/receivedispatch/dispatches") {
    DB localTx { implicit session =>
      SQL(DispatchingTransaction(request.body.intern).toSql).execute().apply()
    }
    Ok
  }
  delete("/receivedispatch/receipts/:id") {
    DB localTx { implicit session => sql"DELETE FROM forms WHERE no = ${params("id")}".update.apply()}
    Ok
  }
  delete("/receivedispatch/dispatches/:id") {
    DB localTx { implicit session => sql"DELETE FROM forms WHERE no = ${params("id")}".update.apply() }
    Ok
  }
  put("/receivedispatch/receipts/:id") {
    DB localTx { implicit session => sql"DELETE FROM forms WHERE no = ${params("id")}".update.apply() }

    DB localTx { implicit session =>
      SQL(ReceivingTransaction(request.body.intern).toSql).execute().apply()
    }
    Ok
  }
  put("/receivedispatch/dispatches/:id") {
    DB localTx { implicit session => sql"DELETE FROM forms WHERE no = ${params("id")}".update.apply() }
    DB localTx { implicit session =>
      SQL(DispatchingTransaction(request.body.intern).toSql).execute().apply()
    }
    Ok
  }

  get("/receivedispatch/lastformnumber") {
    val lastFormNumber: Int = (DB readOnly { implicit session => sql"""
                                                                      |SELECT `paper number`
                                                                      |FROM RECEIVEDREPORT
                                                                      |UNION
                                                                      |SELECT `paper number`
                                                                      |FROM DISPATCHEDREPORT
                                                                      |ORDER BY `paper number` DESC
                                                                      |LIMIT 1""".stripMargin
      .map(rs => rs.int("paper number")).single.apply() }).getOrElse(0)
    lastFormNumber.asJson.spaces4
  }

  get("/receivedispatch/people") {
    val people: List[Person] = DB readOnly { implicit session => sql"SELECT * FROM people".map(rs => Person(rs.string("firstname"), rs.string("surname"), rs.string("nationalidno"))).list.apply() }
    people.asJson.spaces4
  }

  get("/receivedispatch/receipts"){
    val forms: List[Int] = DB readOnly { implicit session => sql"SELECT form FROM received".map(rs => rs.int("form")).list.apply() }
    forms.asJson.spaces4
  }

  get("/receivedispatch/dispatches") {
    val forms: List[Int] = DB readOnly { implicit session => sql"SELECT form FROM dispatched".map(rs => rs.int("form")).list.apply() }
    forms.asJson.spaces4
  }

  get("/receivedispatch/reports/receipts/:personID") {
    val receivedReport: List[report.Received] =
      DB readOnly {
        implicit session =>
          sql"""
               |SELECT *
               |FROM receivedreport
               |WHERE `client` = ${params("personID")}""".stripMargin
            .map(rs => report.Received(
              rs.bigInt("no"),
              rs.int("date"),
              rs.int("paper number"),
              rs.int("qty"),
              rs.string("unit"),
              rs.string("description"),
              rs.double("gweight"),
              rs.double("pweight"),
              rs.double("nweight")))
            .list.apply }
    receivedReport.asJson.spaces4
  }

  get("/receivedispatch/reports/dispatches/:personID") {
    val dispatchedReport: List[report.Dispatched] =
      DB readOnly {
        implicit session =>
          sql"""
               |SELECT *
               |FROM dispatchedreport
               |WHERE `client` = ${params("personID")}""".stripMargin
            .map(rs => report.Dispatched(
              rs.bigInt("no"),
              rs.int("date"),
              rs.int("paper number"),
              rs.int("qty"),
              rs.string("unit"),
              rs.string("description"),
              rs.double("gweight"),
              rs.double("pweight"),
              rs.double("nweight")))
            .list.apply }
    dispatchedReport.asJson.spaces4
  }

  get("/receivedispatch/clients") {
    val clients: List[Person] =
      DB readOnly {
        implicit session =>
          sql"""
                |SELECT DISTINCT nationalidno, firstname, surname
                |FROM
                |people JOIN forms ON people.nationalidno = forms.client""".stripMargin
            .map(rs => Person(
              rs.string("firstname"),
              rs.string("surname"),
              rs.string("nationalidno")))
            .list.apply() }
    clients.asJson.spaces4
  }

  get("/receivedispatch/goods/:clientID") {
    val goods: List[report.Goods] =
      DB readOnly {
        implicit session =>
          sql"""
               |SELECT DISTINCT description, unit
               |FROM
               |goods JOIN forms ON goods.form = forms.no
               |WHERE
               |client = ${params("clientID")}""".stripMargin
            .map(rs => report.Goods(
              rs.string("description"),
              rs.string("unit")))
            .list.apply() }
    goods.asJson.spaces4
  }

  get("/receivedispatch/reports/durational/:clientID") {
    val durationalReport: List[report.Durational] =
      DB readOnly {
        implicit session =>
          sql"""
               |SELECT 'dispatched' as MODE, *
               |FROM
               |dispatchedreport
               |WHERE client = ${params("clientID")}
               |UNION
               |SELECT 'received' as MODE, *
               |FROM
               |receivedreport
               |WHERE client = ${params("clientID")}
               |ORDER BY no, date""".stripMargin
            .map(rs => report.Durational(
              rs.string("mode"),
              rs.bigInt("no"),
              rs.int("date"),
              rs.int("paper number"),
              rs.int("qty"),
              rs.string("unit"),
              rs.string("description"),
              rs.double("gweight"),
              rs.double("pweight"),
              rs.double("nweight")))
            .list.apply }
    durationalReport.asJson.spaces4
  }
}
