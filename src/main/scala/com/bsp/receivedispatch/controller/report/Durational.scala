package com.bsp.receivedispatch.controller.report
import argonaut.Argonaut.casecodec10
import argonaut.CodecJson

case class Durational(mode: String, no: BigInt, date: Int, paperNumber: Int, qty: Int, unit: String, description: String, gWeight: Double, pWeight: Double, nWeight: Double)

object Durational {
  implicit def codec: CodecJson[Durational] = casecodec10(Durational.apply, Durational.unapply)("Mode", "FormNo", "Date", "PaperNumber", "Quantity", "UnitOfMeasurement", "Description", "GrossWeight", "PackageWeight", "NetWeight")
}

