package com.bsp.receivedispatch.controller.report

import argonaut.Argonaut.casecodec9
import argonaut.CodecJson

case class Received(no: BigInt, date: Int, paperNumber: Int, qty: Int, unit: String, description: String, gWeight: Double, pWeight: Double, nWeight: Double)

object Received {
  implicit def codec: CodecJson[Received] = casecodec9(Received.apply, Received.unapply)("FormNo", "Date", "PaperNumber", "Quantity", "UnitOfMeasurement", "Description", "GrossWeight", "PackageWeight", "NetWeight")
}
