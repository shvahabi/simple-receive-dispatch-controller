package com.bsp.receivedispatch.controller.report

import argonaut.Argonaut.casecodec2
import argonaut.CodecJson

case class Goods(description: String, unit: String)

object Goods {
  implicit def codec: CodecJson[Goods] = casecodec2(Goods.apply, Goods.unapply)("Description", "UnitOfMeasurement")
}
