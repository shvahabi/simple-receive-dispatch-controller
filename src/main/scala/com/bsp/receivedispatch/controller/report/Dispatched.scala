package com.bsp.receivedispatch.controller.report

import argonaut.Argonaut.casecodec9
import argonaut.CodecJson

case class Dispatched(no: BigInt, date: Int, paperNumber: Int, qty: Int, unit: String, description: String, gWeight: Double, pWeight: Double, nWeight: Double)

object Dispatched {
  implicit def codec: CodecJson[Dispatched] = casecodec9(Dispatched.apply, Dispatched.unapply)("FormNo", "Date", "PaperNumber", "Quantity", "UnitOfMeasurement", "Description", "GrossWeight", "PackageWeight", "NetWeight")
}
